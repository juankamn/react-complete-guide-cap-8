import React from "react";

import classes from "./Card.module.css";

const Card = (props) => {
  return (
    // to take into account own classes and classes that come when defining
    <div className={`${classes.card} ${props.className}`}>
      {props.children}
      {/* props.children bring the content to put it there */}
    </div>
  );
};

export default Card;
