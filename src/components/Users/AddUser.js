import React, { useState, useRef } from "react";

import Card from "../UI/Card";
import Button from "../UI/Button";
import classes from "./AddUser.module.css";
import ErrorModal from "../UI/ErrorModal";
import Wrapper from "../Helpers/Wrapper";

const AddUser = (props) => {
  const nameInputRef = useRef();
  const ageInputRef = useRef();

  // const [enteredUsername, setEnteredUsername] = useState("");
  // const [enteredAge, setEnteredAge] = useState("");
  const [error, setError] = useState();

  const addUserHandler = (event) => {
    event.preventDefault();
    const enteredUsername = nameInputRef.current.value;
    const enteredAge = ageInputRef.current.value;
    if (enteredUsername.trim().length === 0 || enteredAge.trim().length === 0) {
      setError({
        title: "Invalid input",
        message: "Please enter a valid name and age (non-empty values).",
      });
      return;
    }

    if (+enteredAge < 1) {
      setError({
        title: "Invalid age",
        message: "Please enter a valid age (> 0).",
      });
      return;
    }

    const userData = {
      id: Math.random().toString(),
      name: enteredUsername,
      age: enteredAge,
    };

    props.onAddUser(userData);

    nameInputRef.current.value = '';
    ageInputRef.current.value = '';

    // setEnteredUsername("");
    // setEnteredAge("");
  };

  // const usernameChangeHandler = (event) => {
  //   setEnteredUsername(event.target.value);
  // };

  // const ageChangeHandler = (event) => {
  //   setEnteredAge(event.target.value);
  // };

  const errorHandler = () => {
    setError(null);
  };

  return (
  <Wrapper>
    {error && <ErrorModal title={error.title} message={error.message} onConfirm={errorHandler}></ErrorModal>}
    <Card className={classes.input}>
      <form onSubmit={addUserHandler}>
        <label htmlFor="username">Username</label>
        <input
          id="username"
          type="text"
          // value={enteredUsername}
          // onChange={usernameChangeHandler}
          ref={nameInputRef}
        />
        <label htmlFor="age">Age (Years)</label>
        <input
          id="age"
          type="number"
          // value={enteredAge}
          // onChange={ageChangeHandler}
          ref={ageInputRef}
        />
        <Button type="submit">Add User</Button>
      </form>
    </Card>
  </Wrapper>
  );

  // return [
  //   error && (
  //     <ErrorModal
  //       key="add-user-error-modal"
  //       title={error.title}
  //       message={error.message}
  //       onConfirm={errorHandler}
  //     ></ErrorModal>
  //   ),
  //   <Card key="add-user-card" className={classes.input}>
  //     <form onSubmit={addUserHandler}>
  //       <label htmlFor="username">Username</label>
  //       <input
  //         value={enteredUsername}
  //         id="username"
  //         type="text"
  //         onChange={usernameChangeHandler}
  //       />
  //       <label htmlFor="age">Age (Years)</label>
  //       <input
  //         value={enteredAge}
  //         id="age"
  //         type="number"
  //         onChange={ageChangeHandler}
  //       />
  //       <Button type="submit">Add User</Button>
  //     </form>
  //   </Card>,
  // ];
};

export default AddUser;
