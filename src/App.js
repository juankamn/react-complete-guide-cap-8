import React, { useState } from "react";

import AddUser from "./components/Users/AddUser";
import UsersList from "./components/Users/UsersList";


const DUMMY_USERS = [
  {
    id: "u1",
    name: "Alberto Enriquez",
    age: 25,
  },
  {
    id: "u2",
    name: "Dora Montaño",
    age: 45,
  },
];

const App = (props) => {
  const [users, setUsers] = useState(DUMMY_USERS);

  console.log(users);

  const addUserHandler = (enteredUser) => {
    setUsers((prevUsers) => {
      return [...prevUsers, enteredUser];
    });
    console.log(users);
  };

  return (
    <>
      <AddUser onAddUser={addUserHandler}/>
      <UsersList users={users}/>
    </>
  );
}

export default App;
